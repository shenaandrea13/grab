<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Report;
use Illuminate\Http\Request;

class SummaryController extends Controller
{
    public function reportReport(Request $request)
    {
        $data = $request->validate([
            'start' => 'filled',
            'end' => 'filled',
        ]);

        if (array_key_exists('start', $data) && array_key_exists('end', $data)) {
            $report = Report::where('report_date', '>=', $data['start'])
                ->where('report_date', '<=', $data['end'])->get();
            $start = $data['start'];
            $end = $data['end'];
        } else {
            $report = Report::all();
            $start = '';
            $end = '';
        }

        return view('admin.reports.summary', [
            'report_list' => $report, 'start' => $start, 'end' => $end
        ]);
    }

}