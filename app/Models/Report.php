<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;
    protected $fillable = [
        'report_date',
        'picture',
        'report',
        'status',
        'student_id',
        'created_at',
        'update_at',
    ];
}
