<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $fillable = [
        'phone',
        'name',
        'grade',
        'user_id',
        'created_at',
        'update_at',
    ];

}
