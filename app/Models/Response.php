<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    use HasFactory;
    protected $fillable = [
    'report_id',
    'responses_date',
    'responses',
    'staff_id',
    'created_id',
    'update_id',
    ];
}
