<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\Admin\StudentController as AdminStudentController;
use App\Http\Controllers\Admin\ResponseController as AdminResponseController;
use App\Http\Controllers\Admin\ReportController as AdminReportController;
use App\Http\Controllers\Admin\SummaryController as AdminSummaryController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\Staff\UserController as StaffUserController;
use App\Http\Controllers\Staff\StudentController as StaffStudentController;
use App\Http\Controllers\Staff\ResponseController as StaffResponseController;
use App\Http\Controllers\Staff\ReportController as StaffReportController;

use App\Http\Controllers\Student\ResponseController as StudentResponseController;
use App\Http\Controllers\Student\ReportController as StudentReportController;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    Route::middleware(['level:admin'])->group(function () {
        Route::get('/admin', function () {
            return view('admin.home');
        });
        Route::resource('/admin/users', AdminUserController::class);
        Route::resource('/admin/students', AdminStudentController::class);
        Route::resource('/admin/reports', AdminReportController::class);
        Route::resource('/admin/responses', AdminResponseController::class);
        Route::get('/admin/report-report', [AdminSummaryController::class, 'reportReport']);
    });
    Route::middleware(['level:staff'])->group(function () {
        Route::get('/staff', function () {
            return view('staff.home');
        });
        Route::resource('/staff/users', StaffUserController::class);
        Route::resource('/staff/students', StaffStudentController::class);
        Route::resource('/staff/responses', StaffResponseController::class);
        Route::resource('/staff/reports', StaffReportController::class);
    });
    Route::middleware(['level:student'])->group(function () {
        Route::get('/student', function () {
            return view('student.home');
        });
        Route::resource('/student/reports', StudentReportController::class);
        Route::resource('/student/responses', StudentResponseController::class);
    });


    Route::get('/admin', function () {
        return view('admin.home');
    });

    Route::get('/staff', function () {
        return view('staff.home');
    });

    Route::get('/student', function () {
        return view('student.home');
    });

    Route::get('/', function () {
        return view('home');
    });

    Route::get('/aboutbullying', function () {
        return view('aboutbullying');
    });

    Route::get('/report', function () {
        return view('report');
    });
});
Route::get('/login', [LoginController::class, 'show'])->name('login');
Route::post('/login', [LoginController::class, 'check']);
Route::get('/logout', [LoginController::class, 'logout']);
Route::get('/register', [RegisterController::class, 'index']);
Route::post('/register', [RegisterController::class, 'store']);
Route::post('/responses', [ResponseController::class, 'index']);
