@extends('app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-6">
            <h4>Tentang Bullying</h4>
            <h5>Bullying (dalam bahasa Indonesia dikenal sebagai “penindasan/risak”)
                Merupakan segala bentuk penindasan atau kekerasan yang dilakukan dengan sengaja oleh satu orang atau sekelompok orang yang lebih kuat atau berkuasa terhadap orang lain, dengan tujuan untuk menyakiti dan dilakukan secara terus menerus.</h5>
        </div>
        <div class="col-6">
            <img class="" src="img/Bullying-rafiki.png" width="250">
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <img class="" src="img/Bullying-bro.png" width="250">
        </div>
        <div class="col-6">
            <h1>Bentuk-Bentuk Bullying</h1>
            <h4>Physical Bullying</h4>
            <h5>Berupa pukulan, menendang, menampar, meludahi atau segala bentuk kekerasan yang menggunakan fisik.</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <h4>Physical Bullying</h4>
            <h5>Beberapa perilaku verbal bullying adalah:
                ,Perkataan kasar dan tidak sopan.
                ,Menjadikan teman sebagai bahan lelucon dan di luar batas kewajaran.
                ,Meledek teman yang memiliki suatu kelemahan fisik atau karakter.
                ,Menertawakan seseorang secara berlebihan.</h5>
        </div>
        <div class="col-6">
            <img class="" src="img/Bullying-PANA.png" width="250">
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <img class="" src="img/Racism-amico.png" width="250">
        </div>
        <div class="col-6">
            <h4>Relational Bullying</h4>
            <h5>Berupa pengabaian, pengucilan, cibiran dan segala bentuk tindakan untuk mengasingkan seseorang dari komunitasnya. Anak yang mengalami pengucilan biasanya akan lebih sering terlihat sendiri.</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <h4>Cyber Bullying</h4>
        <h5>Segala bentuk tindakan yang dapat menyakiti orang lain dengan saran media elektronik (rekaman video intimidasi, pencemaran nama baik lewat media sosial).</h5>
        </div>
        <div class="col-6">
            <img class="" src="img/Bullying-amico.png" width="250">
        </div>
    </div>
    <body>
        <h1>News about bullying</h1>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/XQJASala1UY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
      </body>
      </html>
      <link rel="stylesheet" href="app.css" type="app.css">
      <div class="card">
        <img src="/img/Racism-amico.png" class="card-img-top" alt="...">
        <div class="card-body">
            <div class="card-title"><h4>Relational Bullying</h4></div>
            <p class="card-text">Pengucilan</p>
            <a href="https://www.malasngoding.com/card-bootstrap-4/" class="btn btn-primary">Lihat</a>
        </div>
    </div>
    <div class="card">
        <img src="/img/Bullying-bro.png" class="card-img-top" alt="...">
        <div class="card-body">
            <div class="card-title"><h4>Physical Bullying</h4></div>
            <p class="card-text">Perundungan Fisik</p>
            <a href="https://www.malasngoding.com/card-bootstrap-4/" class="btn btn-primary">Lihat</a>
        </div>
    </div>
    <div class="card">
        <img src="/img/Bullying-pana.png" class="card-img-top" alt="...">
        <div class="card-body">
            <div class="card-title"><h4>Verbal Bullying</h4></div>
            <p class="card-text">Perundungan dengan Kata-Kata</p>
            <a href="https://www.malasngoding.com/card-bootstrap-4/" class="btn btn-primary">Lihat</a>
        </div>
    </div>
    <div class="card">
        <img src="/img/Racism-amico.png" class="card-img-top" alt="...">
        <div class="card-body">
            <div class="card-title"><h4>Cyber Bullying</h4></div>
            <p class="card-text">Perundungan di Dunia Maya</p>
            <a href="https://youtu.be/4mrE5zgEvt4" class="btn btn-primary">Lihat</a>
        </div>
    </div>
      @endsection