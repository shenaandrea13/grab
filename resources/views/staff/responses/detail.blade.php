@extends('app')

@section('content')
    <div class="container">
        <h1>Detail Report</h1>
        <form action="/staff/responses/{{ $response->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-12 mb-3">
                    <label for="report_date" class="form-label">Report_date</label>
                    <input type="text" class="form-control" id="report_date" name="report_date"
                        value="{{ $response->report_date }}">
                </div>
                <div class="col-12 mb-3">
                    <label for="picture" class="form-label">Picture</label>
                    <input type="file" class="form-control" id="picture" name="picture">
                    <img src="{{ asset('storage/' . $response->picture) }}" style="width: 300pax">
                </div>
                <div class="col-12 mb-3">
                    <label for="report" class="form-label">Report</label>
                    <input type="text" class="form-control" id="report" name="report" value="{{ $response->report }}">
                </div>
                <div class="col-12 mb-3">
                    <label for="status" class="form-label">Status</label>
                    <input type="text" class="form-control" id="status" name="status" value="{{ $response->status }}">
                </div>
                <div class="col-12 mb-3">
                    <label for="student_id" class="form-label">Student_id</label>
                    <input type="text" class="form-control" id="student_id" name="student_id"
                        value="{{ $response->student_id }}">
                </div>
                <button type="submit" class="btn btn-warning">Save</button>
                <button type="reset" class="btn btn-danger">Reset</button>
            </div>
        </form>
    </div>
@endsection
