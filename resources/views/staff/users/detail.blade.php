@extends('app')

@section('content')
    <div class="container">
        <h1>Detail User</h1>
        <form action="/staff/users/{{ $user->id }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <label for="username" class="form-label">Username</label>
                <input type="text" class="form-control" id="username" name="username" value="{{ $user->username }}"
                    disabled>
            </div>
            <div class="col-3 mb-3">
                <label for="password" class="form-label">Password</label>
                <input type="password" class="form-control" id="password" name="password">
            </div>
            <div class="col-3 mb-3">
                <label class="form-label">Level</label>
                <select name="level" class="form-select">
                    @foreach (['admin', 'staff', 'student'] as $item)
                        <option value="{{ $item }}" {{ $user->level == $item ? 'selected' : '' }}>
                            {{ $item }}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-warning">Save</button>
            <button type="reset" class="btn btn-danger">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
