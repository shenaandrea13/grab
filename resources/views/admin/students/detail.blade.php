@extends('app')


@section('content')
    <div class="container">
        <h1>Detail Student</h1>
        <form action="/admin/students/{{ $student->id }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-10 mb-3">
                    <label for="phone" class="form-label">Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone" value="{{ $student->phone }}">
                </div>
                <div class="col-10 mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $student->name }}">
                </div>
                <div class="col-10 mb-3">
                    <label for="phone" class="form-label">Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone" value="{{ $student->phone }}">
                </div>
                <div class="col-10 mb-3">
                    <label for="grade" class="form-label">Grade</label>
                    <input type="text" class="form-control" id="grade" name="grade" value="{{ $student->grade }}">
                </div>
                <div class="col-10 mb-3">
                    <label for="user_id" class="form-label">User_id</label>
                    <input type="text" class="form-control" id="user_id" name="user_id"
                        value="{{ $student->user_id }}">
                </div>
                <button type="submit" class="btn btn-warning">Save</button>
                <button type="reset" class="btn btn-danger">Reset</button>
            </div>
        </form>
    </div>
@endsection
