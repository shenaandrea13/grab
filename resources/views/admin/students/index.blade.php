@extends('app')

@section('content')
    <div class="container">
        <h1></h1>
        <p>{{ $student_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Phone</th>
                    <th>Name</th>
                    <th>Grade</th>
                    <th>User_id</th>
                    <th>Created_at</th>
                    <th>Update_at</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($student_list as $student)
                    <tr>
                        <td>{{ $student->id }}</td>
                        <td>{{ $student->phone }}</td>
                        <td>{{ $student->name }}</td>
                        <td>{{ $student->grade }}</td>
                        <td>{{ $student->user_id }}</td>
                        <td>{{ $student->created_at }}</td>
                        <td>{{ $student->updated_at }}</td>
                        <td>
                            <a href="/admin/students/{{ $student->id }}" class="btn btn-primary">Detail</a>
                            <a href="#" class="btn btn-danger" data-bs-toggle="modal"
                          data-bs-target="#modal-{{ $student->id }}">Delete</a>
                    </tr>
                    <div class="modal fade" id="modal-{{ $student->id }}" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Confirm</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                </div>
                                <div class="modal-body">
                                    <p>Student with ID {{ $student->id }} will be deleted.</p>
                                    <p>Are you sure want to delete it?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="/admin/students/{{ $student->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                        <button type="button" class="btn btn-success"
                                            data-bs-dismiss="modal">Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        </tbody>
        </table>
        <a href="/admin/students/create" class="btn btn-success mb-3">Create New</a>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
        <a href="/admin/students/create" class="btn btn primary">Create New</a>
    </div>
@endsection
