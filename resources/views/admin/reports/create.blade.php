@extends('app')

@section('content')
    <div class="container">
        <h1>Detail Report</h1>
        <form action="/admim/reports" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row flex-column">
                <div class="col-12 mb-3">
                    <label for="report_date" class="form-label">Report_date</label>
                    <input type="date" class="form-control" id="report_date" name="report_date">
                </div>
                <div class="col-12 mb-3">
                    <label for="picture" class="form-label">Picture</label>
                    <input type="file" class="form-control" id="picture" name="picture" accept="image/png,image/jpeg">
                </div>
                <div class="col-12 mb-3">
                    <label for="status" class="form-label">Status</label>
                    <input type="text" class="form-control" id="status" name="status">
                </div>
                <div class="col-12 mb-3">
                    <label for="report" class="form-label">Report</label>
                    <input type="text" class="form-control" id="report" name="report">
                </div>
                <div class="col-12 mb-3">
                    <label for="student_id" class="form-label">Student_id</label>
                    <input type="text" class="form-control" id="student_id" name="student_id">
                </div>


            </div>

            <button type="submit" class="btn btn-success">Simpan</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
