@extends('app')

@section('content')
    <div class="container">
        <h1>Laporan Pengaduan Bullying</h1>
        <form action="/admin/report" method="GET">
            <div class="row">
                <label for="start" class="col-1 col-form-label">Dari</label>
                <div class="col-2">
                    <input type="date" class="form-control" id="start" name="start" value="{{ $start }}">                    
                </div>
                <label for="end" class="col-1 col-form-label">Sampai</label>
                <div class="col-2">
                    <input type="date" class="form-control" id="end" name="end" value="{{ $end }}">
                </div>
                <div class="col-1">
                    <button type="submit" class="btn btn-success">Cari</button>
                </div>
            </div>
        </form>
        <table class="table">
            <thead>
                <tr>
                    <th>Report_date</th>
                    <th>Report</th>
                    <th>Picture</th>
                    <th>Status</th>
                    <th>Student_id</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($report_list as $report)
                    <tr>
                        <td>{{ $report->report_date}}</td>
                        <td>{{ $report->report}} </td>
                        <td>{{ $report->picture }}</td>
                        <td>{{ $report->status }}</td>
                        <td>{{ $report->student_id }}</td>
                        <td>
                        </td>
                    </tr>
                    <div class="modal fade" id="modal-{{ $report->id }}" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Konfirmasi</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                </div> 
                                <div class="modal-body">
                                    <p>Complaint dengan ID {{ $report->id }} akan dihapus.</p>
                                    <p>Lanjutkan?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="/admin/reportReport{{ $report->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Batal</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        <button type="button" class="btn btn-secondary" onclick="window.print()">
            Print
        </button>
    </div>
@endsection
