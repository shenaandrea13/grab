@extends('app')

@section('content')
<div class="container">
    <h1></h1>
    <p>{{ $report_list->links() }}</p>
    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Report_date</th>
                <th>Picture</th>
                <th>Report</th>
                <th>Status</th>
                <th>student_id</th>
                <th>Created_at</th>
                <th>Update_at</th>
            </tr>
        </thead>
        <tbody></th>
                <th>Created
            @foreach ($report_list as $report)
            <tr>
             <td>{{ $report->id }}</td>
             <td>{{ $report->report_date }}</td>
             <td>{{ $report->picture }}</td>
             <td>{{ $report->report }}</td>
             <td>{{ $report->status }}</td>
             <td>{{ $report->student_id }}</td>
             <td>{{ $report->created_at }}</td>
             <td>{{ $report->update_at }}</td>
             <td>
                <a href="/admin/reports/{{ $report->id }}" class="btn btn-primary">Detail</a>
                <a href="#" class="btn btn-danger"data-bs-toggle="modal"
                data-bs-target="#modal-{{ $report->id }}">Delete</a>
            </tr>   
            <div class="modal fade" id="modal-{{ $report->id }}" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Confirm</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                        </div>
                        <div class="modal-body">
                            <p>User with ID {{ $report->id }} will be deleted.</p>
                            <p>Are you sure want to delete it?</p>
                        </div>
                        <div class="modal-footer">
                            <form action="/admin/reports/{{ $report->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                                <button type="button" class="btn btn-success"
                                    data-bs-dismiss="modal">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach 
        </tbody>
    </table>
    <a href="/admin/reports/create" class="btn btn-primary">Tambah</a>
    @if ($errors->any())
    @foreach ($errors->all() as $error)
        <p class="text-danger">{{ $error }}</p>
    @endforeach
@endif
</div>
@endsection