@extends('app')

@section('content')
    <div class="container">
        <h1>Detail Response</h1>
        <form action="/admin/responses" method="POST">
            @csrf
            <div class="row flex-column">
                <div class="col-12 mb-3">
                    <label for="report_id" class="form-label">Report_id</label>
                    <input type="text" class="form-control" id="report_id" name="report_id">
                </div>
                <div class="col-10 mb-3">
                    <label for="responses_date" class="form-label">Responses_date</label>
                    <input type="date" class="form-control" id="response_date" name="responses_date">
                </div>
                <div class="col-10 mb-3">
                    <label for="responses" class="form-label">responses</label>
                    <input type="text" class="form-control" id="responses" name="responses">
                </div>
                <div class="col-10 mb-3">
                    <label for="staff_id" class="form-label">Staff_id </label>
                    <input type="text" class="form-control" id="staff_id" name="staff_id">
                </div>
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="reset" class="btn btn-secondary">Reset</button>
            </div>

        </form>

        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif

    </div>
@endsection


