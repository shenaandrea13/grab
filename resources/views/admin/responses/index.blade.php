@extends('app')

@section('content')
<div class="container">
    <h1></h1>
    <p>{{ $response_list->links() }}</p>
    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Report_id</th>
                <th>Responses_date</th>
                <th>Response</th>
                <th>staff_id</th>
                <th>Created_at</th>
                <th>Update_at</th>
            </tr>
        </thead>
        <tbody></th>
                <th>Created
            @foreach ($response_list as $response)
            <tr>
             <td>{{ $response->id }}</td>
             <td>{{ $response->report_id }}</td>
             <td>{{ $response->response_date }}</td>
             <td>{{ $response->responses }}</td>
             <td>{{ $response->staff_id }}</td>
             <td>{{ $response->created_at }}</td>
             <td>{{ $response->update_at }}</td>
             <td>
                <a href="/admin/responses/{{ $response->id }}" class="btn btn-primary">Detail</a>
                <a href="#" class="btn btn-danger"data-bs-toggle="modal"
                data-bs-target="#modal-{{ $response->id }}">Delete</a>
            </tr>   
            <div class="modal fade" id="modal-{{ $response->id }}" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Confirm</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                        </div>
                        <div class="modal-body">
                            <p>User with ID {{ $response->id }} will be deleted.</p>
                            <p>Are you sure want to delete it?</p>
                        </div>
                        <div class="modal-footer">
                            <form action="/admin/responses/{{ $response->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                                <button type="button" class="btn btn-success"
                                    data-bs-dismiss="modal">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach 
        </tbody>
    </table>
    <a href="/admin/responses/create" class="btn btn-primary">Tambah</a>
    @if ($errors->any())
    @foreach ($errors->all() as $error)
        <p class="text-danger">{{ $error }}</p>
    @endforeach
@endif
</div>
@endsection  