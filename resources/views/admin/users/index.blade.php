@extends('app')

@section('content')
    <div class="container">
        <h1>Data Users</h1>
        <p>{{ $user_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>id</th>
                    <th>username</th>
                    <th>password</th>
                    <th>level</th>
                    <th>action</th>
                    <th>Created_at</th>
                    <th>Update_at</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($user_list as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->password }}</td>
                        <td>{{ $user->level }}</td>
                        <td>{{ $user->action }}</td>
                        <td>{{ $user->created_at }}</td>
                        <td>{{ $user->update_at }}</td>
                        <td>
                            <a href="/admin/users/{{ $user->id }}" class="btn btn-warning">Detail</a>
                            <a href="#" class="btn btn-danger" data-bs-toggle="modal"
                                data-bs-target="#modal-{{ $user->id }}">Delete</a>
                        </td>
                    </tr>
                    <div class="modal fade" id="modal-{{ $user->id }}" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Confirm</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                </div>
                                <div class="modal-body">
                                    <p>User with ID {{ $user->id }} will be deleted.</p>
                                    <p>Are you sure want to delete it?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="/admin/users/{{ $user->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                        <button type="button" class="btn btn-success"
                                            data-bs-dismiss="modal">Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        <a href="/admin/users/create" class="btn btn-success mb-3">Create New</a>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
