@extends('app')

@section('content')
    <div class="container">
        <h1></h1>
        <p>{{ $report_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Report_id</th>
                    <th>Picture</th>
                    <th>Report</th>
                    <th>Status</th>
                    <th>student_id</th>
                    <th>Created_at</th>
                    <th>Update_at</th>
                </tr>
            </thead>
            <tbody>
                <th>
                    @foreach ($report_list as $report)
                        <tr>
                            <td>{{ $report->id }}</td>
                            <td>{{ $report->report_id }}</td>
                            <td>{{ $report->picture }}</td>
                            <td>{{ $report->report }}</td>
                            <td>{{ $report->status }}</td>
                            <td>{{ $report->student_id }}</td>
                            <td>{{ $report->created_at }}</td>
                            <td>{{ $report->update_at }}</td>
                        <td>
                        <a href="/student/reports/{{ $report->id }} " class="btn btn-primary">Detail</a>
                        </tr>
                    @endforeach
                </th>
            </tbody>
        </table>
        <a href="/student/reports/create" class="btn btn-primary">Tambah</a>
    </div>
@endsection
