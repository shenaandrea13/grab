@extends('app')

@section('content')
    <div class="container">
        <h1>Detail Response</h1>
        <form action="/student/responses{{ $response->id }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="responses_date" class="form-label">Responses_date</label>
                    <input type="date" class="form-control" id="response_date" name="responses_date"
                        value="{{ $response->response_date }}">
                </div>
                <div class="col-10 mb-3">
                    <label for="response" class="form-label">Response</label>
                    <input type="text" class="form-control" id="response" name="response"
                        value="{{ $response->response }}">
                </div>
                <div class="col-3 mb-3">
                    <label for="staff_id" class="form-label">Staff_id </label>
                    <input type="text" class="form-control" id="phone" name="staff_id"
                        value="{{ $response->staff_id }}">
                </div>
                <button type="submit" class="btn btn-warning">Save</button>
                <button type="reset" class="btn btn-danger">Reset</button>
            </div>
        </form>
    @endsection
