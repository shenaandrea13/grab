@extends('app')

@section('content')
<div class="container">
    <h1></h1>
    <p>{{ $response_list->links() }}</p>
    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Report_id</th>
                <th>Responses_date</th>
                <th>Response</th>
                <th>staff_id</th>
                <th>Created_at</th>
                <th>Update_at</th>
            </tr>
        </thead>
        <tbody></th>
                <th>Created
            @foreach ($response_list as $response)
            <tr>
             <td>{{ $response->id }}</td>
             <td>{{ $response->report_id }}</td>
             <td>{{ $response->response_date }}</td>
             <td>{{ $response->responses }}</td>
             <td>{{ $response->staff_id }}</td>
             <td>{{ $response->created_at }}</td>
             <td>{{ $response->update_at }}</td>
             <td>
                
            </tr>   
            @endforeach 
        </tbody>
    </table>
</div>
@endsection  