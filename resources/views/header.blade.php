<?php
$level = auth()->user()->level;
?>

<header class="p-3 mb-3 border-bottom" id=navigation-bar>
    <div class="container d-flex align-item-center">
        <a href="#" class="d-flex align-items-center me-3 text-decoration-none”) <img src="/img/logo.png"
            style="width: 40px">
            <span class="fs-3 ms-0">GRAB</span>
        </a>
        <ul class="nav me-auto">
            <li><a href="/" class="nav-link">Home</a></li>
            <li><a href="/aboutbullying" class="nav-link">About Bullying</a></li>

            <li><a href="/student/reports/create" class="nav-link">Report</a></li>
        </ul>
        <ul class="nav">
            <a href="#" class="dropdown-toggle text-decoration-none" data-bs-toggle="dropdown">
                <i class="bi bi-person-circle" style="font-size: 20px"></i>
            </a>
            <ul class="dropdown-menu">
                <li><A href="#" class="dropdown-item">Halo, friends</A></li>
                <li>
                    <hr class="dropdown-divider">
                </li>
                @if ($level != 'student')
                    <li><a href="/{{ $level }}/users" class="dropdown-item">Users</a></li>
                    <li><a href="/{{ $level }}/students" class="dropdown-item">Students</a></li>
                @endif
                <li><a href="/{{ $level }}/responses" class="dropdown-item">Responses</a></li>
                <li><a href="/{{ $level }}/reports" class="dropdown-item">Reports</a></li>
                
                @if ($level == 'admin')
                <li><a href="/{{ $level }}/report-report" class="dropdown-item">Report-report</a></li>
                @endif
                <li><a href="/logout" class="dropdown-item">Log out</a></li>
            </ul>
            {{-- <li class="nav-item">
                <a href="/logout" class="nav-link">Logout</a>
            </li> --}}
        </ul>
    </div>
</header>
