<!DOCTYPE html>
<html lang="en">
  <head>
    <title>GRAB</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width", initial-scale=1">
    <link rel="stylesheet" href="style.css" />
    <link
      rel="stylesheet"
      type="text/css"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
    />
  </head>
  <body>
    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="footer-col">
            <h4>About us</h4>
            <ul>
              <li><a href="#">About us</a></li>
              <li><a href="#">Contact us</a></li>
              <li><a href="#">Our service</a></li>
            </ul>
          </div>
          <div class="footer-col">
            <h4>About bullying</h4>
            <ul>
              <li><a href="#">Pelaporan Tindak Bullying</a></li>
              <li><a href="#">Edukasi Pencegahan Bullying</a></li>
              <li><a href="#">Tindak Lanjut Kasus Bullying</a></li>
            </ul>
          </div>
          <div class="footer-col">
            <h4>Help</h4>
            <ul>
              <li><a href="#">Care</a></li>
              <li><a href="#">Privacy Policy</a></li>
              <li><a href="#">Terms and Condition</a></li>
            </ul>
          </div>
          <div class="footer-col">
            <h4>Follow us on</h4>
            <div class="social-media">
              <a href="#"><i class="fab fa-facebook"></i></a>
              <a href="#"><i class="fab fa-instagram"></i></a>
              <a href="#"><i class="fab fa-twitter"></i></a>
              <a href="#"><i class="fab fa-youtube"></i></a>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <div class="fixed-footer">
      <div class="container">GRAB (Gracia Anti Bullying)</div>
    </div>
  </body>
</html>