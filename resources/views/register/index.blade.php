@extends('app')

@section('content')
<div class="row justify-content-center">
  <div class="col-md-5">

     @if(session()->has('success'))
          <div class="alert alert-success alert-dismissible fade show mt-5" role="alert">
              {{ session('success') }}
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
      @endif

     @if(session()->has('loginError'))
          <div class="alert alert-danger alert-dismissible fade show mt-5" role="alert">
              {{ session('loginError') }}
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
      @endif

      <main class="form-signin">
          <h1 class="h3 mb-3 fw-normal text-center">Login</h1>
          <form class="form-signin" action="/login" method="post">
              @csrf
            <div class="form-floating">
              <input type="text" name="username" class="form-control @error('username') is-invalid @enderror" id="username" placeholder="Username" autofocus required value="{{ old('username') }}">
              <label for="username">Username</label>
              @error('username')
              <div class="invalid-feedback">
                  {{ $message }}
              </div>
            </div>
            @enderror
          </div>
          <div class="form-floating">
            <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
            <label for="password">Password</label>
          </div>
          <div class="checkbox mb-3">
            <label>
              <input type="checkbox" value="remember-me"> Remember me
            </label>
          </div>
          <button class="w-100 btn btn-lg btn-primary" type="submit">LOGIN</button>
        </form>
        <small class="d-block text-center">Don't have an account? <a href="/register">Sign Up</a></small>
      </main>
</div>
</div>


@endsection
