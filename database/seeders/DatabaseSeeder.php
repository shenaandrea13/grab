<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Student;
use App\Models\Report;
use App\Models\Response;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $user_6 = User::create([
            'username' => 'marshena',
            'password' => Hash::make('Coba_123'),
            'level' => 'admin'
        ]);

        $user_7 = User::create([
            'username' => 'sereni',
            'password' => Hash::make('Coba_123'),
            'level' => 'staff'
        ]);

        $user_1 = User::create([
            'username' => 'karin',
            'password' => Hash::make('Coba_123'),
            'level' => 'student'
        ]);

        $student_1 = Student::create([
            'phone' => '098537235',
            'name' => 'karin',
            'grade' => 'XII',
            'user_id' => $user_1->id
        ]);

        $report_1 = Report::create([
             'report_date' => '2023-03-15',
             'picture' => '',
             'report' => 'saya dipukul regina pada saat belajar coding',
             'status' => 'processed',
             'student_id' => $user_1->id
        ]);

        $response_1 = Response::create([
            'responses_date' => '2023-03-16',
            'responses' => 'Terima kasih sudah menghubungi kami',
            'report_id' => $report_1->id,
            'staff_id' => $user_1->id
        ]);
        
        $user_2 = User::create([
            'username' => 'regina',
            'password' => Hash::make('Coba_123'),
            'level' => 'student'
        ]);

        $student_2 = Student::create([
            'phone' => '08727282828',
            'name' => 'regina',
            'grade' => 'XII',
            'user_id' => $user_2->id
        ]);

        $report_2 = Report::create([
             'report_date' => '2023-03-19',
             'picture' => '',
             'report' => 'saya dibully oleh bryan karena jatuh',
             'status' => 'new',
             'student_id' => $user_2->id
        ]);

        $response_2 = Response::create([
            'responses_date' => '2023-03-20',
            'responses' => 'Terima kasih sudah menghubungi kami',
            'report_id' => $report_2->id,
            'staff_id' => $user_2->id
        ]);

        $user_3 = User::create([
            'username' => 'amel',
            'password' => Hash::make('Coba_123'),
            'level' => 'student'
        ]);

        $student_3 = Student::create([
            'phone' => '098537235',
            'name' => 'amel',
            'grade' => 'XII',
            'user_id' => $user_3->id
        ]);

        $report_3 = Report::create([
             'report_date' => '2023-03-16',
             'picture' => '',
             'report' => 'saya ditonjok oleh grace',
             'status' => 'processed',
             'student_id' => $user_3->id
        ]);

        $response_3 = Response::create([
            'responses_date' => '2023-03-17',
            'responses' => 'Terima kasih sudah menghubungi kami',
            'report_id' => $report_3->id,
            'staff_id' => $user_3->id
        ]);
        
        
        $user_4 = User::create([
            'username' => 'catherine',
            'password' => Hash::make('Coba_123'),
            'level' => 'student'
        ]);

        $student_4 = Student::create([
            'phone' => '084934839',
            'name' => 'catherine',
            'grade' => 'XII',
            'user_id' => $user_4->id
        ]);

        $report_4 = Report::create([
             'report_date' => '2023-03-09',
             'picture' => '',
             'report' => 'saya diomongin lewat chat sebab dia menghina tubuh saya',
             'status' => 'processed',
             'student_id' => $user_4->id
        ]);

        $response_4 = Response::create([
            'responses_date' => '2023-03-10',
            'responses' => 'Terima kasih sudah menghubungi kami',
            'report_id' => $report_4->id,
            'staff_id' => $user_4->id
        ]);
        
        $user_5 = User::create([
            'username' => 'hendriawan',
            'password' => Hash::make('Coba_123'),
            'level' => 'student'
        ]);

        $student_5 = Student::create([
            'phone' => '09826227',
            'name' => 'hendriawan',
            'grade' => 'XII',
            'user_id' => $user_5->id
        ]);

        $report_5 = Report::create([
             'report_date' => '2023-03-15',
             'picture' => '',
             'report' => 'saya dipukul regina pada saat belajar coding',
             'status' => 'processed',
             'student_id' => $user_5->id
        ]);

        $response_5 = Response::create([
            'responses_date' => '2023-03-16',
            'responses' => 'Terima kasih sudah menghubungi kami',
            'report_id' => $report_5->id,
            'staff_id' => $user_5->id
        ]);
        

    }
}
